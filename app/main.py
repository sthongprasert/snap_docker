#!/usr/bin/env python
from ml.__init__ import create_app
import argparse

# Gunicorn entry point.
application = create_app()

if __name__ == '__main__':
    # Entry point when run via Python interpreter.
    print("== Running in debug mode ==")
    application.run(host='127.0.0.1', port=8080, debug=True)