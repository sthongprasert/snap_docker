import pandas as pd  
import numpy as np  
import ml.models.svm_oneclass_core as svm_oneclass
import ml.models.tensorflow_core as tensor_core
import ml.models.database_connect as database
#from mysql.connector import MySQLConnection, Error
from pandas.io.json import json_normalize
from datetime import datetime
from bson import Binary, Code
from bson.json_util import dumps, STRICT_JSON_OPTIONS
import json

GBQ_PROJECT = 'gcp-aml-demo'
#TRAIN_DATA_QUERY = 'SELECT * FROM [encryption-project:snap_dataset.transactions_unsupervised_train]'
#TEST_DATA_QUERY = 'SELECT * FROM [encryption-project:snap_dataset.transactions_unsupervised_test]'
FEATURE_COLUMNS_WITH_TARGET_SVM = ["age", "dependent", "TimeSinceLastTransaction", "amount", "distanceDiff", "deceased", "fraud"]
FEATURE_COLUMNS_WITHOUT_TARGET_SVM = ["age", "dependent", "TimeSinceLastTransaction", "amount", "distanceDiff", "deceased"]

FEATURE_COLUMNS_WITH_TARGET_TF = ["age", "dependent", "TimeSinceLastTransaction", "amount", "distanceDiff", "deceased", "fraud", "Not_Fraud"]
FEATURE_COLUMNS_WITHOUT_TARGET_TF = ["age", "dependent", "TimeSinceLastTransaction", "amount", "distanceDiff", "deceased"]
LABEL_COLUMN = 'fraud'
SAVE_SVM_MODEL_LOCATION = './app/svm.pkl'
PREDICT_SVM_MODEL_LOCATION = './svm.pkl'
SAVE_TF_MODEL_LOCATION_DOCKER = './app/tensorflow_linear_regression.ckpt'
PREDICT_TF_MODEL_LOCATION_DOCKER = './tensorflow_linear_regression.ckpt'
SAVE_TF_MODEL_LOCATION_MLE = './app/tmp/'
PREDICT_TF_MODEL_NAME_MLE = 'usda_snap'
GCP_PROJECT = 'gcp-aml-demo'

def predict_data_svm(data):
    
    data = json_normalize(data)
    data_norm = normalized_data(data, FEATURE_COLUMNS_WITHOUT_TARGET_SVM)
    print("Data Norm")
    print(data_norm)
    model = svm_oneclass.load_model_svm_oneclass(PREDICT_SVM_MODEL_LOCATION)
    result = svm_oneclass.predict_model(model, data_norm)
    
    data = data.join(pd.DataFrame({'fraud': result, 'writeTime': datetime.utcnow()}))
    data = data.to_json(orient='records')
    data = json.loads(data)
    #database.writeEntryToDatabaseMongo(data)
    
    
    #data = data.join(pd.DataFrame({'fraud': [result]}))
#    query = "INSERT INTO transaction(transaction_id, card_holder_name, age, snap_billing_address, snap_billing_city \
#                                     snap_billing_state, snap_billing_zip, snap_billing_longitude, snap_billing_latitude,\
#                                     snap_card_number, snap_ccv, snap_expiration_date, person_ssn, gender, dependent,\
#                                     datetime, TimeSinceLastTransaction, amount, merchant, merchant_zip, merchant_longitude,\
#                                     merchant_latitude, deceased, distanceDiff, fraud) \
#                                     VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    
#    transaction_id = pd.DataFrame.get_value(data, 0, 'transaction_id', takeable=False)
#    card_holder_name = pd.DataFrame.get_value(data, 0, 'card_holder_name', takeable=False)
#    age = pd.DataFrame.get_value(data, 0, 'age', takeable=False).astype(int)
#    snap_billing_address = pd.DataFrame.get_value(data, 0, 'snap_billing_address', takeable=False)
#    snap_billing_city = pd.DataFrame.get_value(data, 0, 'snap_billing_city', takeable=False)
#    snap_billing_state = pd.DataFrame.get_value(data, 0, 'snap_billing_state', takeable=False)
#    snap_billing_zip = pd.DataFrame.get_value(data, 0, 'snap_billing_zip', takeable=False).astype(int)
#    snap_billing_longitude = pd.DataFrame.get_value(data, 0, 'snap_billing_longitude', takeable=False)
#    snap_billing_latitude = pd.DataFrame.get_value(data, 0, 'snap_billing_latitude', takeable=False)
#    snap_card_number = pd.DataFrame.get_value(data, 0, 'snap_card_number', takeable=False)
#    snap_ccv = pd.DataFrame.get_value(data, 0, 'snap_ccv', takeable=False).astype(int)
#    snap_expiration_date = pd.DataFrame.get_value(data, 0, 'snap_expiration_date', takeable=False)
#    person_ssn = pd.DataFrame.get_value(data, 0, 'person_ssn', takeable=False)
#    gender = pd.DataFrame.get_value(data, 0, 'gender', takeable=False)
#    dependent = pd.DataFrame.get_value(data, 0, 'dependent', takeable=False)
#    datetime = pd.DataFrame.get_value(data, 0, 'datetime', takeable=False).astype(int)
#    TimeSinceLastTransaction = pd.DataFrame.get_value(data, 0, 'TimeSinceLastTransaction', takeable=False).astype(int)
#    amount = pd.DataFrame.get_value(data, 0, 'amount', takeable=False)
#    merchant = pd.DataFrame.get_value(data, 0, 'merchant', takeable=False)
#    merchant_zip = pd.DataFrame.get_value(data, 0, 'merchant_zip', takeable=False).astype(int)
#    merchant_longitude = pd.DataFrame.get_value(data, 0, 'merchant_longitude', takeable=False)
#    merchant_latitude = pd.DataFrame.get_value(data, 0, 'merchant_latitude', takeable=False)
#    deceased = pd.DataFrame.get_value(data, 0, 'deceased', takeable=False)
#    distanceDiff = pd.DataFrame.get_value(data, 0, 'distanceDiff', takeable=False)
#    print(transaction_id, card_holder_name, age, snap_billing_address, snap_billing_city, snap_billing_state, snap_billing_zip, snap_billing_longitude, snap_billing_latitude, snap_card_number, snap_ccv, 
#            snap_expiration_date, person_ssn, gender, dependent, datetime, TimeSinceLastTransaction, amount, merchant, merchant_zip, merchant_longitude, merchant_latitude, deceased, distanceDiff, fraud)
#    
#    args = (transaction_id, card_holder_name, age, snap_billing_address, snap_billing_city, snap_billing_state, snap_billing_zip, snap_billing_longitude, snap_billing_latitude, snap_card_number, snap_ccv, 
#            snap_expiration_date, person_ssn, gender, dependent, datetime, TimeSinceLastTransaction, amount, merchant, merchant_zip, merchant_longitude, merchant_latitude, deceased, distanceDiff, fraud)
#    database.writeEntryToDatabaseMySQL(query, args)****
    
    return result

def predict_data_tf(data, deploy_type):
    
    data = json_normalize(data)
    data_norm = normalized_data(data, FEATURE_COLUMNS_WITHOUT_TARGET_TF)
    
    if (deploy_type=="mle"):
        result = tensor_core.predict_model_tensor_mle(data_norm, PREDICT_TF_MODEL_NAME_MLE, GCP_PROJECT)
        
    if (deploy_type=="docker"):
        result = tensor_core.predict_model_tensor_docker(data_norm, PREDICT_TF_MODEL_LOCATION_DOCKER)
        
    if (result[0,0] > 0.5):
        fraud = np.array([1.])
    if (result[0,1] > 0.5):
        fraud = np.array([0.])
 
    data = data.join(pd.DataFrame({'fraud': fraud, 'writeTime': datetime.utcnow()}))
    data = data.to_json(orient='records')
    data = json.loads(data)
    #database.writeEntryToDatabaseMongo(data)    
    
    return fraud

def train_model_gbq_tf(query_train, query_test, learning_rate, training_epochs, batch_size, display_step, deploy_type):  
    
    #learning_rate = 0.0001
    #training_epochs = 5000
    #batch_size = 100
    #display_step = 250  
        
    #Query data from GBQ  
    data = database.read_data_from_gbq(GBQ_PROJECT, query_train)
    data_test = database.read_data_from_gbq(GBQ_PROJECT, query_test)
    
    data_feature = normalized_data(data, FEATURE_COLUMNS_WITHOUT_TARGET_TF)
    data_label = normalized_data(data, ['fraud', 'Not_Fraud'])
   
    #data_feature = data.drop('fraud')
    maxsize = len(data_feature[:])
    data_feature_test = normalized_data(data_test, FEATURE_COLUMNS_WITHOUT_TARGET_TF)
    data_label_test = normalized_data(data_test, ['fraud', 'Not_Fraud'])
    #data_feature_test = data_test.drop('fraud')
    #data_label_test = data_test.drop(FEATURE_COLUMNS_WITHOUT_TARGET)
    if (deploy_type=="mle"):
        precision, recall, f1_score, accuracy = tensor_core.train_model_tensor_mle(data_feature, data_label, data_feature_test, data_label_test, learning_rate, training_epochs, batch_size, display_step, maxsize, SAVE_TF_MODEL_LOCATION_MLE)
    
    if (deploy_type=="docker"):
        precision, recall, f1_score, accuracy = tensor_core.train_model_tensor_docker(data_feature, data_label, data_feature_test, data_label_test, learning_rate, training_epochs, batch_size, display_step, maxsize, SAVE_TF_MODEL_LOCATION_DOCKER)
    
    result = {"Accuracy: ": accuracy, "Precision: ": precision, "Recall: ": recall, "F1: ": f1_score}
                    
    return result   
 
def train_model_gbq_svm(query_train, query_test):    
             
    data = database.read_data_from_gbq(GBQ_PROJECT, query_train)
    data_test = database.read_data_from_gbq(GBQ_PROJECT, query_test)
        
    data = normalized_data(data, FEATURE_COLUMNS_WITH_TARGET_SVM)
    data_test = normalized_data(data_test, FEATURE_COLUMNS_WITH_TARGET_SVM)       
    outliers, label = svm_oneclass.create_label_variable(data, LABEL_COLUMN)
    _, label_test = svm_oneclass.create_label_variable(data_test, LABEL_COLUMN)
            
    data.drop([LABEL_COLUMN, 'target'], axis=1, inplace=True)
    data_test.drop([LABEL_COLUMN, 'target'], axis=1, inplace=True)
    print("Confirming shape of Train dataset: ", data.shape)
    print("Confirming shape of Test dataset: ", data_test.shape)
        
    nu = float(outliers.shape[0]) / float(data.shape[0])
    model = svm_oneclass.train_model_svm_oneclass(data, nu, 'rbf', 0.00005, SAVE_SVM_MODEL_LOCATION)
    accuracy, precision, recall, f1, auc = svm_oneclass.evaluate_svm_oneclass_model(model, data, label)
    accuracy_test, precision_test, recall_test, f1_test, auc_test = svm_oneclass.evaluate_svm_oneclass_model(model, data_test, label_test)
        
    result = {"Accuracy: ": accuracy, "Precision: ": precision, "Recall: ": recall, "F1: ": f1, "AUC: ": auc}
            
    return result

def normalized_data(data, feature_columns):
    
    data = data[feature_columns]
    
    if ('age' in data.columns):
        data['age'] = np.log(data['age'])
    
    if ('dependent' in data.columns):
        data['dependent'] = np.log(data['dependent'] + 0.0001)
    
    if ('TimeSinceLastTransaction' in data.columns):
        data['TimeSinceLastTransaction'] = data['TimeSinceLastTransaction']/max(data['TimeSinceLastTransaction'] + 0.0001)
    
    if ('amount' in data.columns):
        data['amount'] = np.log(data['amount'] + 0.0001)

    if ('distanceDiff' in data.columns):
        data['distanceDiff'] = np.log(data['distanceDiff'] + 0.0001)
    
    if ('deceased' in data.columns):
        data.deceased = data.deceased.astype(float)
    
    return data

