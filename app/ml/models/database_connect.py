import os
import requests
import pandas as pd  
from mysql.connector import MySQLConnection, Error
from configparser import ConfigParser
from pymongo import MongoClient

MONGO_DB_URL = "mongodb://35.190.160.69:27017"

def read_data_from_gbq(project_id_input, query_string):
    
    data = pd.read_gbq(query_string, project_id=project_id_input)
    return data

def writeEntryToDatabaseMySQL(query, data):
    
    try:
        #Connecting to MySQL Database
        db_config = read_db_config_mysql()
        conn = MySQLConnection(user='root', password='Deloitte1', host='localhost', database='snap_transaction')
        cursor = conn.cursor()
        cursor.execute(query, data)
        if conn.is_connected():
            print('connection established.')
        else:
            print('connection failed.')       
        
        conn.commit()
    except Error as error:
        print(error)
    
    finally:
        cursor.close()
        conn.close()
        print('Connection closed')
    
def read_db_config_mysql(filename='C:\\Users\\sthongprasert\\Desktop\\EclipseWorkspace\\USDA_SNAP_ML\\app\\models\\mysql_config.ini', section='mysql'):
    #Read database configuration file and return a dictionary object
    #:param filename: name of the configuration file
    #:param section: section of database configuration
    #:return: a dictionary of database parameters
    
    # create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(filename)
    print(parser.has_section(section))
 
    # get section, default to mysql
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))
    
    return db

def writeEntryToDatabaseMongo(data):
    try:

        client = MongoClient(MONGO_DB_URL)
        db = client.snap_transaction
        db.transactions.insert(data)
        
    finally:
        client.close()