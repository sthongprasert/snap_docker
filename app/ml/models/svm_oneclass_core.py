import os
import requests
from sklearn import metrics
from sklearn import svm
from sklearn.externals import joblib

def create_label_variable(data, label_column):
    
    data.loc[data[label_column] == 0, 'target'] = 1
    data.loc[data[label_column] == 1, 'target'] = -1
    label = data['target']
    outliers = label[label == -1]
    
    return outliers, label

def train_model_svm_oneclass(data, nu_value, kernel_value, gamma_value, file_location):
    
    model = svm.OneClassSVM(nu=nu_value, kernel=kernel_value, gamma=gamma_value)
    model.fit(data)
    save_model_svm_oneclass(model, file_location)
    
    return model

def evaluate_svm_oneclass_model(model, data, label):
    
    preds = model.predict(data)  
    targs = label
    
    accuracy = metrics.accuracy_score(targs, preds)
    precision = metrics.precision_score(targs, preds)
    recall = metrics.recall_score(targs, preds)
    f1 = metrics.f1_score(targs, preds)
    auc = metrics.roc_auc_score(targs, preds)
    
    print("accuracy: ", accuracy)  
    print("precision: ", precision)  
    print("recall: ", recall)  
    print("f1: ", f1)  
    print("area under curve (auc): ", auc)
    
    return accuracy, precision, recall, f1, auc

def predict_model(model, data):
    
    return model.predict(data)

def save_model_svm_oneclass(model, file_location):
    
    joblib.dump(model, file_location)
    
def load_model_svm_oneclass(file_location):
    
    model = joblib.load(file_location)
    return model