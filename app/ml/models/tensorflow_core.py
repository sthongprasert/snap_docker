import tensorflow as tf
import numpy as np
import googleapiclient.discovery
import json
from flask import jsonify

def predict_model_tensor_docker(data_feature, model_location):

    feature_columns_num = len(data_feature.columns)
    tf.reset_default_graph()
    x = tf.placeholder(tf.float32, [None, feature_columns_num])   
    y = tf.placeholder(tf.float32, [None, 2]) 
    
    prediction = nn_prediction_equation(feature_columns_num, 2, x)
    cost = tf.reduce_mean(-tf.reduce_sum(y * tf.log(prediction), reduction_indices = [1]), name = 'cost')
    tf.summary.scalar('cost', cost)
    tf.summary.scalar('prediction', prediction)
    optimizer = tf.train.AdamOptimizer(0.0001).minimize(cost)
    saver = tf.train.Saver(max_to_keep=2)
    
    with tf.Session() as sess:
        saver.restore(sess, model_location)
        init = tf.global_variables_initializer()
        sess.run(init)
        
        correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        result = sess.run(prediction, feed_dict = {x: data_feature.values})
        sess.close()
        
    return result
"""    
def predict_model_tensor_mle(data_feature, model_location, project, version=None):

    service = googleapiclient.discovery.build('ml', 'v1')
    name = 'projects/{}/models/{}'.format(project, model_location)
    version = "version1"
    if version is not None:
        name += '/versions/{}'.format(version)
    
    data_feature = data_feature.values.reshape(1, 6)
    data_feature = data_feature.tolist()
    y_feature = [0.0,0.0]
    print(data_feature)
    response = service.projects().predict(
        name=name,
        body={'instances': [{'inputs': [{'x': data_feature, 'y': y_feature}]}]}
    ).execute()

    if 'error' in response:
        raise RuntimeError(response['error'])

    return response['predictions']
"""    
def train_model_tensor_docker(data_feature, data_label, data_feature_test, data_label_test, learning_rate, training_epochs, batch_size, display_step, maxsize, model_location):
 
    feature_columns_num = len(data_feature.columns)
    label_columns_num = len(data_label.columns)

    x = tf.placeholder(tf.float32, [None, feature_columns_num])   
    y = tf.placeholder(tf.float32, [None, label_columns_num]) 
    
    prediction = nn_prediction_equation(feature_columns_num, label_columns_num, x)
    cost = tf.reduce_mean(-tf.reduce_sum(y * tf.log(prediction), reduction_indices = [1]), name = 'cost')
    tf.summary.scalar('cost', cost)
    tf.summary.scalar('prediction', prediction)
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
    saver = tf.train.Saver(max_to_keep=2)
    
    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        
        correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    
        #Each training cycle
        for epoch in range(training_epochs):
            total_batch = int(maxsize/batch_size)
            
            for i in range(total_batch):
                
                sess.run(optimizer, feed_dict={x:data_feature.iloc[batch_size*i:batch_size*(i+1), :], y:data_label.iloc[batch_size*i:batch_size*(i+1), :]})
                
            if (epoch + 1 ) % 50 == 0:
                print("Prediction: ", sess.run(prediction, feed_dict = {x: data_feature_test.values}))

            if (epoch + 1) % display_step == 0:
                print('Cost for Epoch: ', epoch, sess.run(cost, feed_dict={x:data_feature.iloc[batch_size*i:batch_size*(i+1), :], y:data_label.iloc[batch_size*i:batch_size*(i+1), :]}))
                feed_dict = {x: data_feature_test.values, y: data_label_test.values.reshape(len(data_label_test.values), 2)}
                print("Train Accuracy: ", sess.run(accuracy, feed_dict={x: data_feature.values, y: data_label.values.reshape(len(data_label.values), 2)}))
                tf_confusion_metrics(prediction, y, sess, feed_dict)
                
                
        precision, recall, f1_score, accuracy = tf_confusion_metrics(prediction, y, sess, feed_dict)
        save_path = saver.save(sess, model_location)
    return (precision, recall, f1_score, accuracy)   
"""
def train_model_tensor_mle(data_feature, data_label, data_feature_test, data_label_test, learning_rate, training_epochs, batch_size, display_step, maxsize, model_location):

      
    feature_columns_num = len(data_feature.columns)
    label_columns_num = len(data_label.columns) 
    
    x = tf.placeholder(tf.float32, [None, feature_columns_num], name = 'x')   
    y = tf.placeholder(tf.float32, [None, label_columns_num], name = 'y') 
    values, indices = tf.nn.top_k(y, 2)
    prediction = nn_prediction_equation(feature_columns_num, label_columns_num, x)
    cost = tf.reduce_mean(-tf.reduce_sum(y * tf.log(prediction), reduction_indices = [1]), name = 'cost')   
    optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)
    builder = tf.saved_model.builder.SavedModelBuilder(model_location)
    
    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        correct_prediction = tf.equal(tf.argmax(prediction, 1), tf.argmax(y, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        #Build signature_def_map

        #Each training cycle
        for epoch in range(training_epochs):
            total_batch = int(maxsize/batch_size)
            
            for i in range(total_batch):
                
                sess.run(optimizer, feed_dict={x:data_feature.iloc[batch_size*i:batch_size*(i+1), :], y:data_label.iloc[batch_size*i:batch_size*(i+1), :]})

            if (epoch + 1) % display_step == 0:
                print('Cost for Epoch: ', epoch, sess.run(cost, feed_dict={x:data_feature.iloc[batch_size*i:batch_size*(i+1), :], y:data_label.iloc[batch_size*i:batch_size*(i+1), :]}))
                feed_dict = {x: data_feature_test.values, y: data_label_test.values.reshape(len(data_label_test.values), 2)}
                print("Train Accuracy: ", sess.run(accuracy, feed_dict={x: data_feature.values, y: data_label.values.reshape(len(data_label.values), 2)}))
                tf_confusion_metrics(prediction, y, sess, feed_dict)
            
        inputs = {"x": tf.saved_model.utils.build_tensor_info(x), "y": tf.saved_model.utils.build_tensor_info(y)}
        outputs = {"prediction": tf.saved_model.utils.build_tensor_info(prediction)} 
        predict_signature = tf.saved_model.signature_def_utils.build_signature_def(
            inputs = inputs,
            outputs = outputs,
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME
        )
               
        classification_inputs = tf.saved_model.utils.build_tensor_info(x)
        classification_output_class = tf.saved_model.utils.build_tensor_info(y)
        classification_output_score = tf.saved_model.utils.build_tensor_info(values)
        classification_signature = (
            tf.saved_model.signature_def_utils.build_signature_def(
                inputs = {tf.saved_model.signature_constants.CLASSIFY_INPUTS: classification_inputs},
                outputs = {tf.saved_model.signature_constants.CLASSIFY_OUTPUT_CLASSES: classification_output_class,
                           tf.saved_model.signature_constants.CLASSIFY_OUTPUT_SCORES: classification_output_score},
                method_name=tf.saved_model.signature_constants.CLASSIFY_METHOD_NAME
            )
        )  
        
        legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
        builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.SERVING], 
            signature_def_map={
                'predict_fraud': predict_signature, tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: classification_signature
            }, 
            legacy_init_op=legacy_init_op)
        
        builder.save()       
        precision, recall, f1_score, accuracy = tf_confusion_metrics(prediction, y, sess, feed_dict)
    
        return (precision, recall, f1_score, accuracy)
"""    
def tf_confusion_metrics(model, actual_classes, session, feed_dict):
    predictions = tf.argmax(model, 1)
    actuals = tf.argmax(actual_classes, 1)
    
    ones_like_actuals = tf.ones_like(actuals)
    zeros_like_actuals = tf.zeros_like(actuals)
    ones_like_predictions = tf.ones_like(predictions)
    zeros_like_predictions = tf.zeros_like(predictions)
    
    tp_op = tf.reduce_sum(
        tf.cast(tf.logical_and(tf.equal(actuals, ones_like_actuals), 
        tf.equal(predictions, ones_like_predictions)), tf.float32))

    tn_op = tf.reduce_sum(
        tf.cast(tf.logical_and(tf.equal(actuals, zeros_like_actuals), 
        tf.equal(predictions, zeros_like_predictions)), tf.float32))

    fp_op = tf.reduce_sum(
        tf.cast(tf.logical_and(tf.equal(actuals, zeros_like_actuals), 
        tf.equal(predictions, ones_like_predictions)), tf.float32))

    fn_op = tf.reduce_sum(
        tf.cast(tf.logical_and(tf.equal(actuals, ones_like_actuals), 
        tf.equal(predictions, zeros_like_predictions)), tf.float32))

    tp, tn, fp, fn = session.run([tp_op, tn_op, fp_op, fn_op], feed_dict=feed_dict)
    print('TP: ', tp)
    print('TN: ', tn)
    print('FP: ', fp)
    print('FN: ', fn)
    tpfn = float(tp) + float(fn)
    tpr = 0 if tpfn == 0 else float(tp)/tpfn
    fpr = 0 if tpfn == 0 else float(fp)/tpfn

    total = float(tp) + float(fp) + float(fn) + float(tn)
    accuracy = 0 if total == 0 else (float(tp) + float(tn))/total

    recall = tpr
    tpfp = float(tp) + float(fp)
    precision = 0 if tpfp == 0 else float(tp)/tpfp
  
    f1_score = 0 if recall == 0 else (2 * (precision * recall)) / (precision + recall)
    
    print('Precision = ', precision)
    print('Recall = ', recall)
    print('F1 Score = ', f1_score)
    print('Accuracy = ', accuracy)
    
    return (precision, recall, f1_score, accuracy)

def nn_prediction_equation(feature_columns_num, label_columns_num, x):
    
    with tf.name_scope('hidden1'):
        w1 = tf.Variable(tf.truncated_normal([feature_columns_num, (feature_columns_num*2)], stddev=0.01), name='weights')
        b1 = tf.Variable(tf.ones([(feature_columns_num*2)]), name='biases')
        hidden_layer_1 = tf.nn.relu(tf.matmul(x, w1) + b1)

    with tf.name_scope('hidden2'):
        w2 = tf.Variable(tf.truncated_normal([(feature_columns_num*2), feature_columns_num], stddev=0.01), name='weights')
        b2 = tf.Variable(tf.ones([feature_columns_num]), name='biases')
        hidden_layer_2 = tf.nn.relu(tf.matmul(hidden_layer_1, w2) + b2)
    
    with tf.name_scope('softmax_linear'):
        w3 = tf.Variable(tf.truncated_normal([feature_columns_num, label_columns_num], stddev=0.01), name='weights')
        b3 = tf.Variable(tf.ones([label_columns_num]), name='biases')
        prediction = tf.nn.softmax(tf.matmul(hidden_layer_2, w3) + b3, name = 'prediction')
    
    return prediction