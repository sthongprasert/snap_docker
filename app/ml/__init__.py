from flask import Flask

def _initialize_blueprints(application):
    
    #Registering Flask for Blueprint
    from ml.views.catchbot import catchbot
    application.register_blueprint(catchbot, url_prefix='/api/v1/catchbot')
    
def create_app():

    #Creating an app by initializing components
    application = Flask(__name__)
    _initialize_blueprints(application)
    
    return application