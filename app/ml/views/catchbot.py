from flask import Blueprint, jsonify, request
import ml.models.catchbot as catchbot_model

catchbot = Blueprint('catchbot', __name__)

@catchbot.route('/predict', methods=['POST'])
def run_predict():
    
    request_data = request.get_json()
    #print({"Model Type Data: ": request_data['Model_Type']})
    #print({"Data: ": request_data['Data']})
    
    model_type = request_data['Model_Type']
    data = request_data['Data']
    
    if (model_type == 'unsupervised_svm'):
        result = catchbot_model.predict_data_svm(data)
    
    if (model_type == 'supervised_tf_logistic_regression_docker'):
        result = catchbot_model.predict_data_tf(data, "docker")
    
    if (model_type == 'supervised_tf_logistic_regression_mle'):
        result = catchbot_model.predict_data_tf(data, "mle")
        
    if (result == 1):
        print("not fraud")
    if (result == -1):
        print("fraud")
      
    status_code = 200
    result = result.tolist()
    response = {'Predicted Result: ': result}

    return jsonify(response), status_code 

#@catchbot.route('/train', methods=['POST'])
#def run_training(model_type, query_train, query_test, learning_rate, training_epochs, batch_size, display_step):
    
    #request_data = request.get_json()
    #print({"Model Type Data: ": request_data['Model_Type']})
    #print({"Query: ": request_data['Query']})
    
    #model_type = request_data['Model_Type']
    #query_train = request_data['Query_Train']
    #query_test = request_data['Query_Test']
    #learning_rate = request_data['']
    #training_epochs = request_data['']
    #batch_size = request_data['']
    #display_step = request_data['']
    
#    if (model_type == 'unsupervised_svm'):
#        result = catchbot_model.train_model_gbq_svm(query_train, query_test, learning_rate, training_epochs, batch_size, display_step)  
    
#    if (model_type == 'supervised_tf_logistic_regression'):
#        result = catchbot_model.train_model_gbq_tf(query_train, query_test, learning_rate, training_epochs, batch_size, display_step)
    
       
#    status_code = 200
#    response = {'Result: ': result}

#    return jsonify(response), status_code 