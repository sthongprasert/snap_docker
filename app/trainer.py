from ml.__init__ import create_app
import argparse
import ml.models.catchbot as catchbot_models
import sys

FLAGS = None

def main(_):

    print(FLAGS.model_type)
    print("Printed FLAGS")
    if (FLAGS.model_type == 'unsupervised_svm'):
        result = catchbot_models.train_model_gbq_svm(FLAGS.train_query, FLAGS.test_query)
    if (FLAGS.model_type == 'supervised_tf_logistic_regression'):
        result = catchbot_models.train_model_gbq_tf(FLAGS.train_query, FLAGS.test_query, FLAGS.learning_rate, FLAGS.training_epochs, FLAGS.batch_size, FLAGS.display_step, FLAGS.deployment_type)
    
    print("Training result: ", result)
    
if __name__ == '__main__':
    # Entry point when run via Python interpreter.
    print("== Running in debug mode ==")
    parser = argparse.ArgumentParser()
    parser.register("type", "bool", lambda v: v.lower() == "true")
    parser.add_argument(
        "--model_type",
        type = str,
        default = "unsupervised_svm",
        help = "Valid model types: {'unsupervised_svm', 'supervised_tf_logistic_regression'}."
    )
    parser.add_argument(
        "--train_query",
        type = str,
        default = "N/A",
        help = "This requires a valid SQL query for Training dataset."
    )
    parser.add_argument(
        "--test_query",
        type = str,
        default = "N/A",
        help = "This requires a valid SQL query for Test dataset."
    )  
    parser.add_argument(
        "--learning_rate",
        type = float,
        default = 0.0001,
        help = "This requires a valid double for learning rate."
    ) 
    parser.add_argument(
        "--training_epochs",
        type = int,
        default = 5000,
        help = "This requires a valid integer for training epochs."
    ) 
    parser.add_argument(
        "--batch_size",
        type = int,
        default = 100,
        help = "This requires a valid integer for batch size."
    ) 
    parser.add_argument(
        "--display_step",
        type = int,
        default = 250,
        help = "This requires a valid integer for display step."
    ) 
    parser.add_argument(
        "--deployment_type",
        type = str,
        default = "docker",
        help = "This is the deployment step field. Options are 'mle' or 'docker'"
    ) 
    
    FLAGS, unparsed = parser.parse_known_args()
    main([sys.argv[0]] + unparsed)
    #application.run(main=main, argv=[sys.argv[0]] + unparsed)