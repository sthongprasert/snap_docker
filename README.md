# EBT Machine Learning Module
> Machine learning module for USDA SNAP using both supervised tensorflow logistic regression and unsupervised svm SciKit-Learn libraries 

### Developing
```
git clone https://sthongprasert@bitbucket.org/sthongprasert/snap_docker.git

cd snap_docker
//The steps below will include setting up virtual environment to ensure that the system is not impacted by the libraries
mkdir venv
virtualenv venv
source venv/bin/activate
```

### Run Docker
```
cd ~/snap_docker

//Build docker container for the app. App is named 'catchbot_app'
sudo docker build -t catchbot_app .

//Running 'catchbot_app' image 
sudo docker run -d -p 80:80 -t catchbot_app
```

### Stop  & Terminate Docker
```
//Find docker container id
docker ps -a
docker stop <container_id>
docker rm <container_id>
```

### To run the container in Kubernetes
```
//This section assumes that cluster is already created. This assumes that model has been trained and will be deployed for prediction.

//Run container build command
sudo docker build -t catchbot_app:v1

//List image to make sure it is there
sudo docker images

//Deploy the application
sudo kubectl run <Kubectl_Service_Name> --image=catchbot_app:v1 --port 80

//Display pods
sudo kubectl get pods

//Expose application (--port parameter can be changed to exposed on a different port)
sudo kubectl expose deployment <Kubectl_Service_Name> --type=LoadBalancer --port 80 --target-port 80

//Get the external IP address
sudo kubectl get service

//Scale up Kubernetes to additional pods (--replicas can be adjusted to desired pods number) by either using manual scale or autoscale based on CPU percentage
kubectl scale deployment <Kubectl_Service_Name> --replicas=3
kubectl autoscale deployment <Deployment_Name> --min=3 --max=5 --cpu-percent=80

//To apply newer update, create a new image build with newer version (:v2)
sudo kubectl set image deployment/<Kubectl_Service_Name> <Kubectl_Service_Name>=catchbot_app:v2
```

### To run this application on Google Container Engine (GKE)
```
//This section assumes that container cluster is already created and all of the commands are run in GCP CLI. This assumes that model has been trained and will be deployed for prediction.

//Export project name
export PROJECT_ID="$(gcloud config get-value project -q)"

//Run container build command
docker build -t gcr.io/${PROJECT_ID}/catchbot-app:v1 

//List image to make sure it is there
docker images

//Push docker image to registry
gcloud docker -- push gcr.io/${PROJECT_ID}/catchbot-app:v1

//Assuming GKE cluster is created, this will get the necessary credential to deploy to it
gcloud container clusters get-credentials <GKE_Cluster_Name>

//Deploy the application
kubectl run <Kubectl_Service_Name> --image=gcr.io/${PROJECT_ID}/catchbot-app:v1 --port 80

//Display pods
kubectl get pods

//Expose application (--port parameter can be changed to exposed on a different port)
kubectl expose deployment <Kubectl_Service_Name> --type=LoadBalancer --port 80 --target-port 80

//Get the external IP address
kubectl get service

//Scale up Kubernetes to additional pods (--replicas can be adjusted to desired pods number) by either using manual scale or autoscale based on CPU percentage
kubectl scale deployment <Kubectl_Service_Name> --replicas=3
kubectl autoscale deployment <Deployment_Name> --min=3 --max=5 --cpu-percent=80

//To apply newer update, create a new image build with newer version (:v2)
kubectl set image deployment/<Kubectl_Service_Name> <Kubectl_Service_Name>=catchbot_app:v2



//To clean up, delete service and cluster
kubectl delete service <Kubectl_Service_Name>
gcloud container clusters delete <GKE_Cluster_Name>
```

### To access prediction and training models
```
//Make HTTP Post request using Postman or SoapUI to url 'http://ip:80/api/v1/catchbot/predict'

//To train, activate /app/trainer.py script with --model_type, --train_query, --test_query parameters

python app/trainer.py --model_type "unsupervised_svm" --train_query "SELECT * FROM [gcp-aml-demo:snap_dataset.transactions_train]" --test_query "SELECT * FROM [gcp-aml-demo:snap_dataset.transactions_test]" --deployment_type docker
python app/trainer.py --model_type "supervised_tf_logistic_regression" --train_query "SELECT * FROM [gcp-aml-demo:snap_dataset.transactions_train]" --test_query "SELECT * FROM [gcp-aml-demo:snap_dataset.transactions_test]" --deployment_type docker

//After training a model, a new docker build is required to perform prediction with new model
```